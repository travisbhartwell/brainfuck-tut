0.5.1.3

* Add changelog

0.5.1.2

* Add homepage, issue tracker, project repo

0.5.1.1

* Fix documentation formatting issues

0.5.1.0

* Initial release
