# Brainfuck: A Toy Implementation

This project exists to show what a
[Brainfuck](http://en.wikipedia.org/wiki/Brainfuck) evaluator might
look like in Haskell.

The implementation is fairly well documented. I endeavored to make it
readable.

## Terms

Below is the abstract syntax tree for the BF language as implemented:

```haskell
data Term
  = IncDP        -- >
  | DecDP        -- <
  | OutDP        -- ?
  | IncByte      -- +
  | DecByte      -- -
  | OutByte      -- .
  | InByte       -- ,
  | JumpForward  -- [
  | JumpBackward -- ]
  deriving (Show, Eq)
```

## Evaluation Semantics

I followed the summary given on the wikipedia page closely. A few
particulars to this implementation:

* "Jump not found" errors abort evaluation and return the state of the tape
* Out of bound errors are not detected
* Evaluation proceeds until the instruction stream runs out

## Executable

An executable is provided:

```
$ cabal build bfh
$ cabal run bfh
usage: bfh <size> <program>
$ cabal run bfh 10 ',.'
a<enter>
a
```
